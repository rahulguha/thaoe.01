/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 22/2/14
 * Time: 6:16 AM
 * To change this template use File | Settings | File Templates.
 */
/* The API controller
 Exports 3 methods:
 * post - Creates a new thread
 * list - Returns a list of threads
 * show - Displays a thread and its posts
 */
var jade = require('jade'),
    fs = require('fs'),
    shortid = require('shortid')
    _ = require('lodash-node');
var mysql = require("./db_connect/mysql.js");
var util = require('./util/util.js');
var email = require('./email/m.js');
var payload = require('./model/mail_payload.js');
var email_request = require('./model/email_request.js');
var db_query =require('./db_queries/query.json');
var logger = util.get_logger("api");
var swig  = require('swig');
var async = require('async');
var mongo = require('./db_connect/mongo.js');

//var Q = require('q');

"use strict";
// init mongo
//var mongo_client = mongo.init("schneider","sales_registry");
//var serverCache = mongo_native();

//********************  get  methods   *************************************
exports.get_email_requests = function (req, res) {

    email_request.get_request()
        .then (
        function (results){
            send_to_response(results,res);
        },
        function (err){
            send_to_response(err,res);
        }

    );

    //var r = async.compose(get_email_requests ());



};
exports.send_email_batch = function (req, res) {


    // get email requests
    email_request.get_request()
        .then (
        function (results){
            logger.info("Start Email delivery") ;
            async.each(results,
                function (r, callback) {
                    logger.info("Sending Email to: " + r.to[0].email);
                    email_request.change_request_status(r.id, 1) // 1 = pick up
                        .then (
                            function (){
                                    logger.info("picked up: " + r.id + "-" + r.to[0].email)

                                    callback();
                                },
                                function (err){
                                    send_to_response(err,res);
                                }
                            )


                },
                function (err) {
                    if (err) {
                        // One of the iterations produced an error.
                        // All processing will now stop.
                        console.log('query error');
                        res.send ("query error");
                    } else {
                        console.log("all emails sent")
                        res.send ("email sent");
                    }


                }
            )
        },
        function (err){
            send_to_response(err,res);
        }

    );



};

//******************** end  get *************************************

//********************  post methods   *************************************

// This inserts a record in email_request collection which will later be picked up for processing
// This method caters to to the simple case where all data are provided by the post message
exports.insert_email_request = function (req, res) {
    try{
        insert_email_request(req,res);
    }
    catch (exception){
        logger.info(exception);
    }

};
exports.insert_email_request_with_query = function (req, res) {
    try {

        // parse query
        var q = req.body.query;
        var q_arr_mysql= [];
        var  q_arr_mongo = [];
        var result_arr = [];
        // we start with query object in request that has two parts
        // One is for mysql which is used for getting TO
        // second is to get data elements
        // for now data element is assumed to be coming from Mongo
        for (var i = 0; i < q.length; i++) {
            if (util.get_source_by_query_purpose(q[i].purpose).source == 'mysql'){
                q_arr_mysql.push(
                    {
                        "source": 'mysql',
                        "target_field": q[i].target_field,
                        "query": q[i].query
                    }
                )
            } else{
                q_arr_mongo.push(
                    {
                        "source": 'mongo',
                        "target_field": q[i].target_field,
                        "query": q[i].query
                    }
                )
            }
        }

        // get to data based on post query. in this version we support only MySQL user_registration.
        // But in future even this can come from request
        async.each(q_arr_mysql,
            function (q, callback) {
                logger.info("MySQL Query Firing: " + q.query);
                var conn = mysql.get_mysql_connection();
                    conn.query(q.query,
                        function (err, rows) {
                            if (err) {
                                logger.info(err);
                                res.end();
                            }
                            result_arr.push({"target_field": q.target_field, "address": rows});
                            callback();
                        });
            },
            function (err) {
                if (err) {
                    console.log('query error');
                } else {
                    logger.info('query successful-data received');
                    // now push them to email request structure to make sure they get added
                    // we support only TO now. Not sure if we ever need bcc/cc to support
                    var to = [];
                    for (var i = 0; i < result_arr.length; i++) {
                        for (var j = 0; j < result_arr[i].address[0].length; j++) {
                            to.push(
                                [{"name": result_arr[i].address[0][j].full_name, "email": result_arr[i].address[0][j].email_address }]
                            );
                        }
                    }
                    logger.info("to' array created");

                    //we need to call mongo query for each of "to" record email.
                    // assumption is - "email" field in to array is used to query mongo database
                    async.each(q_arr_mongo,
                        function (q, callback) {
                            logger.info("Fire  mongo query: " + q.query);
                            var query_id = q.query;
                            var selected_query = '';
                            // get query definition from file
                            // important point - query id needs to come
                            // as part of request which will be looked up
                            for (var i=0; i< db_query.length; i++){
                                if (db_query[i].id == query_id){
                                    selected_query = db_query[i].query;
                                    break;
                                }
                            }
                            var arr_data_query = [];
                            var arr_data = [];
                            // query need to have a $match and and $and criteria.
                            // also it needs to have user_id as first criteria.
                            // you can have anything below that
                            for (var i=0; i< to.length; i++){
                                var tmp = _.clone(selected_query, true);
                                tmp[0].$match.$and[0].user_id = to[i][0].email;
                                arr_data_query.push(tmp);
                                tmp = null;
                            }
                            // presently mongo.init is hardcoded to connect to host and port defined in config
                            // but we support request level db and collection selection
                            mongo.init(req.body.db, req.body.collection, function (error, mongo_client) {
                                if (error)
                                    throw error;
                                else {
                                    async.each(arr_data_query,
                                        function (a_d_q, callback) {
                                            logger.info("data query: " + JSON.stringify( a_d_q));
                                            var mongo_coll = mongo_client.collection("sales_registry")
                                            mongo_coll.aggregate(a_d_q,
                                                function(err, results){
                                                    if (err){
                                                        return err;
                                                    }
                                                    else {
                                                        if (results.length > 0){
                                                            arr_data.push(results);
                                                        }
                                                        else {
                                                            arr_data.push([{'user_id':a_d_q[0].$match.$and[0].user_id}]);
                                                        }
                                                        callback();
                                                    }
                                                }
                                            );
                                        },
                                        function (err) {
                                            if (err) {
                                                console.log('query error');
                                                logger.info ("mongo data call error" + err);
                                            } else {
                                                console.log("data calls complete");
                                                logger.info("data calls complete");
                                                logger.info(arr_data);
                                                // prep data for insertion
                                                var arr_ins_data = [];
                                                for (var i=0; i< to.length; i++){
                                                    for (var j=0; j< arr_data.length; j++){
                                                        // this ensures we are in same index key (by user_id)
                                                        //if (arr_data[j].length > 0){
                                                        if (to[i][0].email == arr_data[j][0].user_id){
                                                            // found match
                                                            arr_ins_data.push(
                                                                {
                                                                    "to": to[i][0],
                                                                    "data" : arr_data[j]
                                                                })
                                                        }
                                                    }

                                                }
                                                // insert records to to DB
                                                async.each(arr_ins_data,
                                                    function (a, callback) {
                                                        logger.info("Inserting to mongo: " + a);
                                                        insert_email_request_from_query(req, res, a, arr_data);
                                                        callback();
                                                    },
                                                    function (err) {
                                                        if (err) {
                                                            console.log('query error');
                                                            logger.info (err);
                                                            res.send ("query error");
                                                        } else {
                                                            console.log("all inserts complete")
                                                            res.send ("added");
                                                        }
                                                    }
                                                )
                                            }
                                        }
                                    )

                                }
                            });


                        },
                        function (err) {
                            if (err) {
                                // One of the iterations produced an error.
                                // All processing will now stop.
                                console.log('query error');
                                res.send ("query error");
                            } else {
                                console.log("all mongo calls complete")
                                //res.send ("added");
                            }


                        }
                    )


                }
                ;
            })
    }
        catch (exception){
            logger.info(exception);
        }
    }



//******************** end  post *************************************


// ******************* private helper functions ***********************
var send_to_response = function(results, res ){
    var arr = [];
    results.forEach(function(r){
        arr.push(r)

    });
    res.contentType('application/json');
    res.send(arr);
}
var return_back  = function(results ){
    var arr = [];
    results.forEach(function(claim){
        arr.push(claim)
    });
    return arr;
}
var populate_address = function (a){
    var addresses = [];
    if (a instanceof Array) {
        for (var i = 0; i < a.length; i++ ){
            addresses.push({"name":a[i].name , "email":a[i].email  })
        }
    } else {
        addresses.push([{"name":a.name , "email":a.email  }])
    }
    return addresses;
}
var insert_email_request = function(req, res){

    var r = new email_request();
    r.app_id = req.body.app_id;
    r.id = r.app_id + "-" + shortid.generate();
    r.priority= req.body.priority;
    r.send_status= 0;//req.body.send_status; initial insert will always be in pending state
    r.call_details.event_id= req.body.call_details.event_id;
    r.call_details.data_id= req.body.call_details.data_id;
    r.from.name= req.body.from.name;
    r.from.email= req.body.from.email;
    r.to = populate_address(req.body.to);
    r.cc = populate_address(req.body.cc);
    r.bcc = populate_address(req.body.bcc);
    r.template_id= req.body.template_id;
    r.data =  JSON.stringify(req.body.data);
    r.markModified('data');
    r.save(function (err){
        logger.info("Email Request Added: " + "\n" + r);

    });
}
//var insert_email_request_from_query = function(req, res, to, data){
var insert_email_request_from_query = function(req, res, o){
// this is called when the address fields are read from database
    var r = new email_request();
    r.app_id = req.body.app_id;
    r.id = r.app_id + "-" + shortid.generate();
    r.priority= req.body.priority;
    r.send_status= 0;//req.body.send_status; initial insert will always be in pending state
    r.call_details.event_id= req.body.call_details.event_id;
    r.call_details.data_id= req.body.call_details.data_id;
    r.from.name= req.body.from.name;
    r.from.email= req.body.from.email;
    r.to = populate_address(o.to);
    r.cc = populate_address(req.body.cc);
    r.bcc = populate_address(req.body.bcc);
    r.template_id= req.body.template_id;
    //r.data =  JSON.stringify(req.body.data);
    r.data =  JSON.stringify(o.data);
    r.markModified('data');
    r.save(function (err){
        //logger.info("Email Request Added: " + "\n" + r);
        //res.send ("added");
    });
}
var exec_mongo_call = function(q){
    var result = mongo_native_exec.get_data(q, "schneider", "sales_registry");
    return result;
}

var template_output = function TemplateOutput(templatename,cont){
    var mainPage_tpl = swig.compileFile(__dirname+'/../template/'+templatename);
    var mainPageOutput;
    var obj = {};
    for (var v in cont.body) {
        obj[v] = cont.body[v];
    }
    mainPageOutput = mainPage_tpl(obj);
    return mainPageOutput;
}



// ******************* private helper functions ***********************

