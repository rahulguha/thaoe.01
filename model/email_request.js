/**
 * Created by rahulguha on 18/03/14.
 */


var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , ObjectId = Schema.ObjectID;

var mongo = require('../db_connect/mongoose.js');

var call_detailSchema= new Schema({
    event_id : { type: Number},
    data_id : { type: String, trim: true }
});

var addressSchema = new Schema({
    name : { type: String, trim: true },
    email :  { type: String, trim: true },
    _id : false
});

var email_requestSchema = new Schema(
//    { _id: false },
    {
    id          :   { type: String, trim: true },
    app_id      :   { type: String, trim: true },
    priority    :   { type: Number},
    send_status :   { type: Number},
    call_details: {
        event_id    :   { type: Number},
        data_id     :   { type: String, trim: true },
        id          :   false
                },
    from        :   {
        name : { type: String, trim: true },
        email :  { type: String, trim: true },
        _id : false
    },
    to          :   [addressSchema],
    cc          :   [addressSchema],
    bcc         :   [addressSchema],
    template_id :   { type: String, trim: true },
//    data:  {  any: {}  } // todo - fix it later
    data        :   { type: String, trim: true }
});

// static methods
email_requestSchema.statics.get_request = function(){
    //var claims = [];
    return this.find(
        {send_status : 0}).sort({priority:1}).exec();// Should return a Promise
}

email_requestSchema.statics.change_request_status = function(req_id, status){
    //var claims = [];
    return this.update(
        {'id': req_id},
        {
            'send_status': status // 1= pick up 2= complete 3=failed
        }
    )
        .exec();
}




email_requestSchema.set('collection', 'req_q');

//var conn = mongo.get_mongoose_connection();
//var request_spool = conn.model('email_request_spool',email_requestSchema, "req_spool");
module.exports = mongo.get_mongoose_connection().model('email_request_spool', email_requestSchema, "req_spool");
module.exports = mongo.get_mongoose_connection().model('email_request_req', email_requestSchema, "req_q");