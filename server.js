/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 21/2/14
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */
var express =       require('express'),
    bodyParser      = require('body-parser'),
    routes =        require('./routes.js'),
    config =        require('./config/config.json'),
    util =          require('./util/util.js'),
    _ =             require('lodash-node'),
    cors            = require('cors'),
    exphbs  =          require('express-handlebars');
    ;

// api documentation lib
var docs = require("express-mongoose-docs");
// start logger
var logger =        util.get_logger("server");


// define routers (new in Express 4.0)
var ping_route      =   express.Router(),
    email_route     =   express.Router(),
    help_route      =   express.Router()
    ;




// start app
var app =           express();
app.use(bodyParser());  // for request body
app.use(cors());        // for x-browser


logger.info("express loaded");

// define view engine for help
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// implement ping_route actions

// This is specific way to inject code when this route is called
// This executes for any ping route.
// Similarly we can (and should) implement same for every route
ping_route.use (function(req,res,next){
    logger.info(util.create_routing_log(req.method, req.url, "ping", "PING"));
    // continue doing what we were doing and go to the route
    next();
});
ping_route.get('/', function(req, res){
    res.send('Tahoe is blue ... Water is warm - come on in ');
});
ping_route.get('/check/mongo', function(req, res){
    res.send('Code for checking Mongo connection will be implemented here');
});
ping_route.get('/check/mysql', function(req, res){
    res.send('Code for checking MySql connection will be implemented here');
});
ping_route.post('/check/post', function(req, res){
    res.send('Checking Post Method' + req.body);
});

app.use('/ping', ping_route);
// end ping route


// email route implementation
email_route.use (function(req,res,next){
    logger.info(util.create_routing_log(req.method, req.url, "email", "EMAIL"));
    // continue doing what we were doing and go to the route
    next();
});

email_route.get('/get/requests', function(req, res){
    res.send('Code for checking Email requests');
});
email_route.get('/send/email/batch', function(req, res){
    routes.send_email_batch(req,res);
});
// insert email request
email_route.post('/insert/request', function(req, res){
    routes.insert_email_request(req,res);
});
// insert email request with query
email_route.post('/insert/request/with_query', function(req, res){
    routes.insert_email_request_with_query(req,res);
});

// get email requests
email_route.get('/request/list', function(req, res){
    routes.get_email_requests(req,res);
});


app.use('/email', email_route);
// end email route



// implement help_route actions
help_route.use (function(req,res,next){
    logger.info(util.create_routing_log(req.method, req.url, "help", "HELP"));
    // continue doing what we were doing and go to the route
    next();
});
help_route.get('/', function(req, res){
    var r_list = [];
    get_routes(ping_route.stack,r_list, "ping");
    get_routes(email_route.stack,r_list, "email");
    get_routes(help_route.stack,r_list, "help");
    res.send(r_list);
});
help_route.get('/show', function(req, res){
    var r_list = [];
    get_routes(ping_route.stack,r_list, "ping");
    get_routes(email_route.stack,r_list, "email");
    get_routes(help_route.stack,r_list, "help");
    //res.send(r_list);
    console.log(r_list);
    res.render('show', {'routes': r_list});
});
app.use('/help', help_route);
// end help route



// This function is used to extract route information from globally defined express router
// This will be refactored later for adding more features and making it more generic
var get_routes = function(r, r_list, route_sub_system){
    for (var i=0; i< r.length; i++){
        if (typeof r[i].route != "undefined"){

            r_list.push (
                {
                    'path': "/"+ route_sub_system + r[i].route.path,
                    'method':r[i].route.methods
                }

            )
        }
    }
    return r_list;
}




logger.info("routes are loaded");
docs(app);
app.listen(3000);
logger.info("http server started");
console.log('Listening on port 3000...');
// start mongo connection


