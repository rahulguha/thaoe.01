/**
 * Created with JetBrains WebStorm.
 * User: Rahul
 * Date: 21/2/14
 * Time: 9:06 PM
 * To change this template use File | Settings | File Templates.
 */
//var cat = require('../schema/category.js');
var api = require('./api.js');

exports.insert_email_request = function (req, res) {
    api.insert_email_request(req, res);
};
exports.insert_email_request_with_query = function (req, res) {
    api.insert_email_request_with_query(req, res);
};

exports.get_email_requests = function (req, res) {
    api.get_email_requests(req, res);
};
exports.send_email_batch = function (req, res) {
    api.send_email_batch(req, res);
};
//exports.send_email = function(req, res) {
//    console.log("routes.send_email");
//    api.email(req.body.template);
//    res.send("Email Sent");
//};
//exports.sendmailmongo= function(req, res) {
//    console.log("routes.sendmailmongo");
//    api.sendmailmongo(req.body.ip,req.body.DBcollection,req.body.query,req.body.template);
//    res.send("Email Sent");
//};
//exports.make_schedule= function(req, res) {
//    console.log("routes.sendmailmongo");
//    api.make_schedule(req.body.ip,req.body.DBcollection,req.body.query,req.body.template,req.body.timestamp);
//    res.send("Email Sent");
//};
//exports.do_schedule= function(req, res) {
//    console.log("routes.sendmailmongo");
//    api.do_schedule();
//    res.send("Email Sent");
//};
//
//exports.findAll = function(req, res) {
//    console.log("routes.findAll");
//    res.send([{name:'wine1'}, {name:'wine2'}, {name:'wine3'}]);
//};
//
//exports.findById = function(req, res) {
//    console.log("routes.findbyId");
//    res.send({id:req.params.id, name: "The Name", description: "description"});
//};
//
//exports.cat = function(req, res) {
//    api.cat_list(req,res);
//};
//
//exports.claim = function (req, res) {
//    api.claim_list(req, res);
//};
//exports.claim_by_company = function (req, res) {
//    api.claims_by_company(req, res);
//};
//

//exports.pickup_email_request = function (req, res) {
//    api.pickup(req, res);
//    //console.log("grrr");
//};
//exports.insert_email_request = function (req, res) {
//    api.insert_email_request(req, res);
//    //console.log("grrr");
//};
//// Add new methods for exporting apis *************************************************************************
//exports.login = function(req, res) {
//   // api.cat_list(req,res);
//   //    console.log (req.body);
//    api.user_login(req,res);
//};
//
//
//exports.addclaim = function (req, res) {
//    // api.cat_list(req,res);
//    //    console.log (req.body);
//    api.add_claim(req, res);
//};
//
//// ******************************************************************************************
//
//exports.upload = function(req, res) {
//
//    console.log(req.body);
//    //console.log(aws.generate_signed_url(req.body.filename));
//    aws.upload(req, res);
//    //res.send({id:req.params.id, name: "The Name", description: "description"});
//};